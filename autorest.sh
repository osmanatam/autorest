#!/usr/bin/bash

# Uncomment for debug this script:
#set -x

################################################################################
#
# Bash script that helps to automaticly restores pgBackRest backups and
# tests if latest backup is valid or not.
#
# It creates unit file for specified stanza then it changes Description and PGDATA
# variable, it changes certain PostgreSQL parameters to run in minimum
# hardware requirements because we don't need any performance. It is a test :)
#
# Run this script with "root" Linux user and backup/restore server.
#
# --- Hüseyin Sönmez ---
#
# --- License: PostgreSQL License ---
#################################################################################

SENDMAIL=0
PROCESSMAX=1
LOGDETAIL='info'
PGPORT=5432
MAILSUBJECT=''
MAILFROM='autorest'
ERROR_USAGE_STRING=$"Try $0 '--help' for more information."
MAIN_USAGE_STRING=$"
Usage: $0 [options] [mail] [mode]

Available operation mode (you can't specify remote and backup together):

        -r, --remote                    remote server for restore
        -b, --backup                    backup server for restore
	-M, --mail                      send mail for error and informations

Available remote mode options:

        -s, --stanza NAME               stanza name for restore test (required)
        -d, --data DIRECTORY            data directory to restore (required)
	-p, --pg-port PORT		port for starting PostgreSQL (Default: $PGPORT)
        -H, --repo-host HOSTNAME        database server host (required)
        -D, --repo-path DIRECTORY       repository directory for backup files (required)
        -U, --repo-host-user NAME       repository user for ssh connection (required)
        -P, --process-max NUMBER        max processes to use for uncompress/transfer (Default: $PROCESSMAX)
	-l, --log-detail NAME           specifies log detail. Available options are 'off','error','warn','info','detail' 'debug','trace'. (Default: $LOGDETAIL)

Available backup mode options:

        -s, --stanza NAME               stanza name for restore test (required)
	-d, --data DIRECTORY            data directory to restore (required)
	-P, --process-max NUMBER        max processes to use for uncompress/transfer (Default: $PROCESSMAX)
        -l, --log-detail NAME           specifies log detail. Available options are 'off','error','warn','info','detail' 'debug','trace'. (Default: $LOGDETAIL)

Available mail options (requires remote or backup):

        -T, --mail-to NAME              sets the to address of mail (required)
        -F, --mail-from NAME            sets the from address of mail (required)
        -S, --mail-subject NAME         sets the subject of mail (required)

Other options:

        -V, --version                   output version information, then exit
        --help, --usage                 show this help, then exit"


# Function that help to write information and error messages to log files.
make_log() {
	DAT=$(date +"%F %T.%3N %Z")
	if [ "$1" = 'print' ]; then
		echo "$2"
		return 0;
	elif [ "$1" = 'log' ]; then
		echo "$DAT - $2" >> "$LOG"
		echo "$DAT - $2" >> "$MAILLOG"
		return 0;
	elif [ "$1" = 'detail' ]; then
		echo "$2"
		echo "$DAT - $2" >> "$LOG"
		echo "$DAT - $2" >> "$MAILLOG"
		return 0;
	elif [ "$1" = 'mail' ]; then
		echo "$2"
		echo "$DAT - $2" >> "$LOG"
		return 0;
	else
		T10="ERROR [010]: There is some error when writing in the log file."
		echo "$T10"
		return 1;
	fi
}

# Function that checks parameters belongs to available operation modes.
check_parameters() {
	if [ "$SETUP" -eq 0 ]; then
		if [[ -z "$STANZA" || -z "$PGPATH" || -z "$REPOHOST" || -z "$REPOPATH" || -z "$REPOHOSTUSER" ]]; then
			T0="ERROR [000]: Some parameters is missing for remote mode. $ERROR_USAGE_STRING"
			make_log print "$T0"
			exit 1;
		else
			if [ "$SENDMAIL" -eq 1 ]; then
				if [[ -z "$MAILTO" || -z "$MAILFROM" || -z "$MAILSUBJECT" ]]; then
				       	T0="Some parameters is missing for mail mode. $ERROR_USAGE_STRING"
					      make_log print "$T0"
					      exit 1;
				else
					return 0;
				fi
			elif [ "$SENDMAIL" -eq 0 ]; then
				return 0;
			fi
		fi
	elif [ "$SETUP" -eq 1 ]; then
		if [[ -z "$STANZA" || -z "$PGPATH" ]]; then
			T0="ERROR [000]: Some parameters is missing for backup mode. $ERROR_USAGE_STRING"
			make_log print "$T0"
			exit 1;
		else
			if [ "$SENDMAIL" -eq 1 ]; then
				if [[ -z "$MAILTO" || -z "$MAILFROM" || -z "$MAILSUBJECT" ]]; then
					T0="ERROR [000]: Some parameters is missing for mail mode. $ERROR_USAGE_STRING"
					make_log print "$T0"
					exit 1;
				else
					return 0;
				fi
			elif [ "$SENDMAIL" -eq 0 ]; then
				return 0;
			fi
		fi
	else
		T0="ERROR [000]: Please specify a operation mode. $ERROR_USAGE_STRING"
		make_log print "$T0"
		exit 1;
	fi
}


# Function that initializes log files etc.
initialize() {
	LOGPATH=/var/log/autorest/$STANZA
	LOG=$LOGPATH/autorest-$(date +%b).log
	MAILLOG=$LOGPATH/autorest-mail.log

	if [ ! -f "$PGPATH" ]; then
		mkdir -p -m 700 "$PGPATH"
		chown -R postgres:postgres "$PGPATH"
	fi

	if [ ! -f "$LOGPATH" ]; then
		mkdir -p -m 700 "$LOGPATH"
		chown -R postgres:postgres "$LOGPATH"
	fi

	if [ ! -f "$LOG" ]; then
		touch "$LOG"
		chmod 600 "$LOG"
		chown postgres:postgres "$LOG"
	fi

	if [ "$(ls "$LOGPATH" | wc -l)" = 13 ]; then
		rm -f "$LOGPATH/*"
		touch  "$LOG"
		chmod 600 "$LOG"
		chown postgres:postgres "$LOG"
	fi

	if [ -f "$MAILLOG" ]; then
		rm -f "$MAILLOG"
		touch "$MAILLOG"
		chown -R postgres:postgres "$MAILLOG"
		chmod 600 "$MAILLOG"
		{
			echo ""
			echo "						---------NEW TEST----------"
			echo ""
		} >> "$LOG"
		T1="INFO [001]: Initialize finished successfully."
		make_log detail "$T1"
		return 0;
	elif [ ! -f "$MAILLOG" ]; then
		touch "$MAILLOG"
		chown -R postgres:postgres "$MAILLOG"
		chmod 600 "$MAILLOG"
		{
			echo ""
			echo "						---------NEW TEST----------"
			echo ""
		} >> "$LOG"
		T1="INFO [001]: Initialize finished successfully."
		make_log detail "$T1"
		return 0;
	else
		T1="ERROR [001]: There was some error when initializing!"
		make_log detail "$T1"
		send_mail
		exit 1;
	fi
}


# Function that performs restore depends an operation mode.
perform_restore() {
	case "$1" in
		backup)
			pgbackrest --stanza="$STANZA" --reset-pg1-host --pg1-path="$PGPATH" --log-level-console="$LOGDETAIL" --process-max="$PROCESSMAX" --delta restore >> "$LOG" 2>&1
			STATUS=$?
			if [ $STATUS -eq 0 ]; then
				T2="INFO [002]: Restoring from $STANZA's latest backup is finished successfully."
				make_log detail "$T2"
				return 0;
			elif [ $STATUS -eq 38 ]; then
				T2="ERROR [002]: Unable to $STANZA's restore while PostgreSQL is running!"
				make_log detail "$T2"
				send_mail
				exit 1;
			else
				T2="ERROR [002]: Failed to restore $STANZA's latest backup!"
				make_log detail "$T2"
				send_mail
				exit 1;
			fi
			;;

		remote)
			if pgbackrest --stanza="$STANZA" --repo1-host="$REPOHOST" --repo1-host-user="$REPOHOSTUSER" --repo1-path="$REPOPATH" --pg1-path="$PGPATH" --log-level-console="$LOGDETAIL" --process-max="$PROCESSMAX" --delta restore >> "$LOG" 2>&1; then
				T2="INFO [002]: Restoring from $STANZA's latest backup is finished successfully."
				make_log detail "$T2"
				return 0;
                        elif [ $STATUS -eq 38 ]; then
                                T2="ERROR [002]: Unable to $STANZA's restore while PostgreSQL is running!"
                                make_log detail "$T2"
                                send_mail
                                exit 1;
			else
				T2="ERROR [002]: Failed to restore $STANZA's latest backup!"
				make_log detail "$T2"
				send_mail
				exit 1;
			fi
			;;
	esac
}


# Function that change "postgresql.conf" files for minimum hardware requirements.
change_conf() {
	if [ -e "$PGPATH/postgresql.conf" ]; then
		sed -i -e"s/^cluster_name =.*$/cluster_name = '$STANZA'/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^.cluster_name =.*$/cluster_name = '$STANZA'/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^port =.*$/port = '$PGPORT'/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^.port =.*$/port = '$PGPORT'/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^listen_addresses =.*$/listen_addresses = 'localhost'/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^shared_preload_libraries =.*$/shared_preload_libraries = ''/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^shared_buffers =.*$/shared_buffers = 128MB/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^work_mem =.*$/work_mem = 4MB/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^maintenance_work_mem =.*$/maintenance_work_mem = 64MB/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^wal_buffers =.*$/wal_buffers = -1/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^effective_cache_size =.*$/effective_cache_size = 4GB/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^superuser_reserved_connections =.*$/superuser_reserved_connections = 3/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^archive_mode =.*$/archive_mode = off/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^wal_keep_segments =.*$/wal_keep_segments = 0/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s:^hba_file =.*$:hba_file = '$PGPATH/pg_hba.conf':" "$PGPATH"/postgresql.conf
	  	sed -i -e"s:^ident_file =.*$:ident_file = '$PGPATH/pg_ident.conf':" "$PGPATH"/postgresql.conf
		sed -i -e"s:^data_directory =.*$:data_directory = '$PGPATH':" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^ssl =.*$/ssl = off/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^log_destination =.*$/log_destination = 'stderr'/" "$PGPATH"/postgresql.conf
		sed -i -e"s/^log_filename =.*$/log_filename = 'postgresql-%a.log'/" "$PGPATH"/postgresql.conf
	  	sed -i -e"s/^logging_collector =.*$/logging_collector = on/" "$PGPATH"/postgresql.conf

		# These options for if pg_hba.conf or pg_ident.conf file is not in PostgreSQL's setup directory
	  	# and of course resetting them for security reasons.
		echo 'local all postgres peer' > "$PGPATH/pg_hba.conf"
	  	echo '' > "$PGPATH/pg_ident.conf"
	  	chmod 600 "$PGPATH/pg_hba.conf" "$PGPATH/pg_ident.conf"
		chown postgres:postgres "$PGPATH/pg_hba.conf" "$PGPATH/pg_ident.conf"
		T3="INFO [003]: Changed PostgreSQL parameters."
		make_log detail "$T3"
		return 0;
	elif [ -e "$PGPATH/postgresql.conf" ]; then
		T3="ERROR [003]: "$PGPATH/postgresql.conf" is not exists!"
		make_log detail "$T3"
		exit 1;
	else
		T3="ERROR [003]: There were some error when 'change_conf' function is running!"
		make_log detail "$T3"
		send_mail
		exit 1;
	fi
}


# Function that copies systemd service file depends on PostgeSQL version.
make_systemd() {
	LATESTWAL=$(pgbackrest --stanza="$STANZA" info | grep 'wal archive' | awk '{print $5}' | awk -F '/' '{print $2}')
	PGVERSION=$(cat "$PGPATH"/PG_VERSION)
	if [ -f "/etc/systemd/system/postgresql-$PGVERSION-$STANZA.service" ]; then
		sed -i -e"s:^Description=.*$:Description=PostgreSQL $PGVERSION $STANZA database server automatic backup test unit file:" /etc/systemd/system/postgresql-"$PGVERSION"-"$STANZA".service
		sed -i -e"s:^Environment=PGDATA=.*$:Environment=PGDATA=$PGPATH:" /etc/systemd/system/postgresql-"$PGVERSION"-"$STANZA".service
		systemctl daemon-reload
		T4="INFO [004]: Systemd file postgresql-$PGVERSION-$STANZA is exists. Changing PGPATH and Description variables."
		make_log detail "$T4"
		return 0;
	elif [ ! -f "/etc/systemd/system/postgresql-$PGVERSION-$STANZA.service" ]; then
		if cp -p "/usr/lib/systemd/system/postgresql-$PGVERSION.service" "/etc/systemd/system/postgresql-$PGVERSION-$STANZA.service"; then
			sed -i -e"s:^Description=.*$:Description=PostgreSQL $PGVERSION $STANZA database server automatic backup test unit file:" /etc/systemd/system/postgresql-"$PGVERSION"-"$STANZA".service
			sed -i -e"s:^Environment=PGDATA=.*$:Environment=PGDATA=$PGPATH:" /etc/systemd/system/postgresql-"$PGVERSION"-"$STANZA".service
			systemctl daemon-reload
			T4="INFO [004]: Systemd file postgresql-$PGVERSION-$STANZA created successfully."
			make_log detail "$T4"
			return 0;
		else
			T4="ERROR [004]: Failed to create systemd file postgresql-$PGVERSION-$STANZA.service!"
			make_log detail "$T4"
			send_mail
			exit 1;
		fi
	else
		T4="ERROR [004]: There were some error when 'make_systemd' function is running!"
		make_log detail "$T4"
		send_mail
		exit 1;
	fi
}


# Function that starts and stops service when restore finished successfully.
start_service() {
	if systemctl start postgresql-"$PGVERSION"-"$STANZA".service; then
		T5="INFO [005]: postgresql-$PGVERSION-$STANZA.service started successfully."
		make_log detail "$T5"
    if [ "$PGVERSION" -ge 12 ]; then
      if [ ! -f "$PGPATH"/recovery.signal ]; then
        T6="INFO [006]: recovery.signal file is not found. Exiting.."
        make_log detail "$T6"
        send_mail
        exit 1;
      elif [ -f "$PGPATH"/recovery.signal ]; then
        T6="INFO [006]: recovery.signal file is found. Waiting for archive recovery to complete."
        make_log detail "$T6"
        until [ -f "$PGPATH"/recovery.signal ]; do
          sleep 60
        done
      fi
    elif [ "$PGVERSION" -le 11 ]; then
  		if [ ! -f "$PGPATH"/recovery.conf ]; then
  			T6="INFO [006]: recovery.conf file is not found. Exiting.."
  			make_log detail "$T6"
  			send_mail
  			exit 1;
  		elif [ -f "$PGPATH"/recovery.conf ]; then
  			T6="INFO [006]: recovery.conf file is found. Waiting for archive recovery to complete."
  			make_log detail "$T6"
  			until [ -f "$PGPATH"/recovery.done ]; do
  				sleep 60
  			done
     		 fi
    fi
    if [[ $(grep -c 'archive recovery complete' "$PGPATH"/log/postgresql-"$(date +%a)".log) -eq 1 ]]; then
			if [[ $(grep -c "unable to find $LATESTWAL" "$PGPATH"/log/postgresql-"$(date +%a)".log) -ne 0 ]]; then
				T7="ERROR [007]: There is a problem with WAL archive. Check the logs."
				make_log detail "$T7"
				if systemctl stop postgresql-"$PGVERSION"-"$STANZA".service; then
					T8="INFO [008]: postgresql-$PGVERSION-$STANZA.service stopped successfully."
					make_log detail "$T8"
					return 0;
				else
					T8="ERROR [008]: Failed to stop postgresql-$PGVERSION-$STANZA.service!"
					make_log detail "$T8"
					send_mail
					exit 1;
				fi
			fi
			else
				T7="INFO [007]: PITR is completed successfully."
				make_log detail "$T7"
				if systemctl stop postgresql-"$PGVERSION"-"$STANZA".service; then
					T8="INFO [008]: postgresql-$PGVERSION-$STANZA.service stopped successfully."
					make_log detail "$T8"
					return 0;
				else
					T8="ERROR [008]: Failed to stop postgresql-$PGVERSION-$STANZA.service!"
					make_log detail "$T8"
					send_mail
					exit 1;
				fi
			fi
	else
		T5="ERROR [005]: Failed to start postgresql-$PGVERSION-$STANZA.service!"
		make_log detail "$T5"
		send_mail
		exit 1;
	fi
}

# Function that sends mail about status of restore.
send_mail() {
	if [ $SENDMAIL -eq 1 ]; then
		if mail -s "$MAILSUBJECT" -r "$MAILFROM" "$MAILTO" < "$MAILLOG"; then
			T11="INFO [011]: Mail sent to $MAILTO successfully."
			make_log mail "$T11"
			return 0;
		else
			T11="ERROR [011]: Mail sent to $MAILTO has failed!"
			make_log mail "$T11"
			return 1;
		fi
	else
		return 0;
	fi
}


# Parameter for what to be parsed in input parameters.
TEMP=$(getopt -o rbM:s:d:p:H:D:U:P:l:T:F:S:V --long remote,backup,mail,stanza:,data:,pg-port:,repo-host:,repo-path:,repo-host-user:,process-max:,log-detail:,mail-to:,mail-from:,mail-subject:,version,help,usage -- "$@")
eval set -- "$TEMP"

# Loop for parsing statements
while true ; do
	case "$1" in
		-r|--remote)
                        SETUP=0
                        check_parameters
                        initialize
                        perform_restore remote
                        change_conf
                        make_systemd
                        start_service
                        if send_mail; then
                                exit 0;
                        else
                                exit 1;
                        fi;;
		-b|--backup)
                        SETUP=1
                        check_parameters
                        initialize
                        perform_restore backup
                        change_conf
                        make_systemd
                        start_service
                        if send_mail; then
                                exit 0;
                        else
                                exit 1;
                        fi;;
		-M|--mail)
                        SENDMAIL=1
                        shift;;
		-s|--stanza)
                        STANZA=$2
                        shift 2;;
                -d|--data)
                        PGPATH=$2
                        shift 2;;
		-p|--pg-port)
			PGPORT=$2
			shift 2;;
                -H|--repo-host)
                        REPOHOST=$2
                        shift 2;;
                -D|--repo-path)
                        REPOPATH=$2
                        shift 2;;
                -U|--repo-host-user)
                        REPOHOSTUSER=$2
                        shift 2;;
		-P|--process-max)
			PROCESSMAX=$2
			shift 2;;
		-l|--log-detail)
			LOGDETAIL=$2
			shift 2;;
		-T|--mail-to)
                        MAILTO=$2
                        shift 2;;
                -F|--mail-from)
                        MAILFROM=$2
                        shift 2;;
                -S|--mail-subject)
                        MAILSUBJECT=$2
                        shift 2;;
		-V|--version)
			echo '1.2'
			exit 0;;
		--help|--usage)
			echo "$MAIN_USAGE_STRING"
			exit 0;;
		*)
			echo "$MAIN_USAGE_STRING"
			exit 1;;
	esac
done
