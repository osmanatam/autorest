# AutoRest

Bash script that helps to automaticly restores pgBackRest backups and tests if latest backup is valid or not.

It creates unit file for specified stanza then it changes Description and PGDATA variable, it changes certain PostgreSQL parameters to run in minimum hardware requirements because we don't need any performance. It is a test :)

Run this script with "root" Linux user and backup/restore server.


## INSTALLATION

1. Just clone the git repository and use it :
```bash
git clone https://gitlab.com/huseynsnmz/autorest.git
```

Thats it. :)

## USAGE

Usage: $0 [options] [mail] [mode]

Available operation mode (you can't specify remote and backup together):

        -r, --remote                    remote server for restore
        -b, --backup                    backup server for restore
        -M, --mail                      send mail for error and informations

Available remote mode options:

        -s, --stanza NAME               stanza name for restore test (required)
        -d, --data DIRECTORY            data directory to restore (required)
        -p, --pg-port PORT              port for starting PostgreSQL (Default: 5432)
        -H, --repo-host HOSTNAME        database server host (required)
        -D, --repo-path DIRECTORY       repository directory for backup files (required)
        -U, --repo-host-user NAME       repository user for ssh connection (required)
        -P, --process-max NUMBER        max processes to use for uncompress/transfer (Default: 1)
        -l, --log-detail NAME           specifies log detail. Available options are 'off','error','warn','info','detail' 'debug','trace'. (Default: error)

Available backup mode options:

        -s, --stanza NAME               stanza name for restore test (required)
        -d, --data DIRECTORY            data directory to restore (required)
        -P, --process-max NUMBER        max processes to use for uncompress/transfer (Default: $PROCESSMAX)
        -l, --log-detail NAME           specifies log detail. Available options are 'off','error','warn','info','detail' 'debug','trace'. (Default: error)

Available mail options (requires remote or backup):

        -T, --mail-to NAME              sets the to address of mail (required)
        -F, --mail-from NAME            sets the from address of mail (required)
        -S, --mail-subject NAME         sets the subject of mail (required)

Other options:

        -V, --version                   output version information, then exit
        --help, --usage                 show this help, then exit


## EXAMPLE

You can set cronjob with using your own configuration like this example commands.

In backup server, without mail option:
```bash
./autorest.sh --stanza db-primary --data /var/lib/pgsql/11/db-primary --backup
```

In remote server, without mail option:
```bash
./autorest.sh --stanza db-primary --repo-host 10.132.16.21 --repo-host-user postgres --repo-path /var/lib/pgbackrest --data /var/lib/pgsql/11/db-primary --remote
```


In backup server, with mail option:
```bash
./autorest.sh --stanza db-primary --data /var/lib/pgsql/11/db-primary --mail-from autorest@snmz.org --mail-to huseynsnmz3@gmail.com --subject AutoRest --mail --backup
```

In remote server, with mail option:
```bash
./autorest.sh --stanza db-primary --repo-host 10.132.16.21 --repo-host-user postgres --repo-path /var/lib/pgbackrest --data /var/lib/pgsql/11/db-primary --mail-from autorest@snmz.org --mail-to huseynsnmz3@gmail.com --subject AutoRest --mail --remote
```
